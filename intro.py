import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output


app = dash.Dash(__name__)

# ------------------------------------------------------------------------------
# Import and clean data (importing csv into pandas)
df = pd.read_csv("data_covid_region.csv", encoding='latin-1', header=0)

df.reset_index(inplace=True)
dff = df.copy()
# ------------------------------------------------------------------------------
# App layout


app.layout = html.Div(children=[
    html.Div([
        html.H1("Covid tracker with Dash", style={'text-align': 'center'}),
        html.H2("nombre de cas en France métropolitaine"),
        dcc.Graph(id='covid_national_numbers', figure={})], className="row"),
    html.Div([
        html.Div([
            html.H2("nombre de cas par région"),
            dcc.Dropdown(id="slct_reasons",
                options=[
                    {"label": "Bretagne", "value": "Bretagne"},
                    {"label": "Occitanie", "value": "Occitanie"}],
                multi=False,
                value="Bretagne",
                style={'width': "40%"}
            ),
            html.Br(),
            html.Div(id='output_container', children=[]),
            html.Br(),
            dcc.Graph(id='covid_graph', figure={})], className="six columns"),
        html.Div([
            html.H2("nombre de cas par région"),
            dcc.Dropdown(id="slct_reasons2",
                options=[
                    {"label": "Bretagne", "value": "Bretagne"},
                    {"label": "Occitanie", "value": "Occitanie"}],
                multi=False,
                value="Bretagne",
                style={'width': "40%"}
            ),
            html.Br(),
            html.Div(id='output_container2', children=[]),
            html.Br(),
            dcc.Graph(id='covid_graph2', figure={})], className="six columns")
       ], className="row") 
    ])


# app.css.append_css({"external_url": external_stylesheets})
# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
        [Output(component_id='output_container', component_property='children'),
            Output(component_id='covid_graph', component_property='figure'),
            Output(component_id='covid_graph2', component_property='figure'),
            Output(component_id='covid_national_numbers', component_property='figure')],
        [Input(component_id='slct_reasons', component_property='value'),
        Input(component_id='slct_reasons2', component_property='value')]
        )
def update_graph(option_slctd, option_slctd2):

    container = "Région sélectionnée: {}".format(option_slctd)

    dff = df.copy()
    dff = dff[dff["nomReg"] == option_slctd]
    dff2 = df.copy()
    print(dff2.head())
    df_national = df.copy();
    df_national = df_national.groupby(df_national['jour'], as_index=False).aggregate({'incid_rea': 'sum'})
    print(df_national.head())
    # Plotly Express
    fig = px.bar(
             data_frame=dff,
             x='jour',
             y='incid_rea'
             )

    fig2 = px.bar(
             data_frame=dff2,
             x='jour',
             y='incid_rea'
             )
    national_fig= px.bar(
            data_frame=df_national,
            x='jour',
            y='incid_rea')

    return container, fig, fig2, national_fig


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run_server(debug=True)
